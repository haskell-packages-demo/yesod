# yesod

RESTful web framework built on WAI. https://www.stackage.org/package/yesod

## Official documentation
* [yesodweb.com](https://www.yesodweb.com)
* [*Yesod quick start guide*](https://www.yesodweb.com/page/quickstart)

### Books
* [*Developing Web Apps with Haskell and Yesod*](https://www.yesodweb.com/book)
  * [*Deploying your Webapp*](https://www.yesodweb.com/book/deploying-your-webapp)
* [*Developing Web Apps with Haskell and Yesod, 2nd Edition*
  ](https://www.oreilly.com/library/view/developing-web-apps/9781491915585/)
  2015 Michael Snoyman

## Unofficial documentation
* [*Yesod (web framework)*](https://en.wikipedia.org/wiki/Yesod_(web_framework))
  (Wikipedia)
* [*Easy Haskell Development Setup with Docker*
  ](https://thoughtbot.com/blog/easy-haskell-development-and-deployment-with-docker)
  2019-03, 2015-07 Tony DiPasquale
* [*Starting with Yesod on Arch Linux*
  ](https://techbeat.in/2016/06/23/starting-with-yesod-on-arch-linux.html)
  2016-06 Prabhakar Kumar
